<?php
/**
 * @package RedditPicViewer
 */

namespace Boris\RedditBundle\Twig;

class BorisExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('arrayMultiply', array($this, 'arrayMultiplyFilter')),
        );
    }

    public function arrayMultiplyFilter($values, $multiplier = 100)
    {
        return array_map(function ($value) use ($multiplier) {
            return $value * $multiplier;
        }, $values);
    }

    public function getName()
    {
        return 'boris_extension';
    }
}
