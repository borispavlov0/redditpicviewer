<?php
/**
 * @package RedditPicViewer
 */

namespace Boris\RedditBundle\Service;

use Boris\RedditBundle\Entity\Histogram;
use Boris\RedditBundle\Entity\Picture;

class HistogramService
{
    public function addHistogram(Picture $picture)
    {
        if (sizeof($picture->getHistograms()) != 0) {
            return $picture;
        }

        $histArray = $this->initializeHistArray();
        $im = $this->getImage($picture);

        $imgSize = getimagesize($picture->getPath());

        $imgWidth = $imgSize[0];
        $imgHeight = $imgSize[1];

        $totalPixels = $imgHeight * $imgWidth;

        for ($y = 0; $y < $imgHeight; $y++) {
            for ($x = 0; $x < $imgWidth; $x++) {
                $colorIndex = imagecolorat($im, $x, $y);
                $colorTran = imagecolorsforindex($im, $colorIndex);

                foreach ($colorTran as $color => $value) {
                    $histArray[$color][$this->getBracket($value)]++;
                }
            }
        }

        foreach ($histArray as $color => $values) {
            foreach ($values as $key => $v) {
                $histArray[$color][$key] = $histArray[$color][$key] / $totalPixels;
            }

            $hist = new Histogram();
            $hist->exchangeArray($histArray[$color]);
            $hist->setColor($color);
            $hist->setPicture($picture);
            $picture->addHistogram($hist);
        }

        return $picture;
    }

    /**
     * @param $value
     *
     * @return int
     * @throws \Exception
     */
    private function getBracket($value)
    {

        for ($i = 0; $i <= 5; $i++) {
            if ($value >= ($i * 51) && $value < (($i + 1) * 51)) {
                return $i;
            }
        }

        throw new \Exception('Value is ' . $value);
    }

    /**
     * @return array
     */
    private function initializeHistArray()
    {
        $histArray = [
            'red'   => [
                0 => 0,
                1 => 0,
                2 => 0,
                3 => 0,
                4 => 0,
                5 => 0
            ],
            'green' => [
                0 => 0,
                1 => 0,
                2 => 0,
                3 => 0,
                4 => 0,
                5 => 0
            ],
            'blue'  => [
                0 => 0,
                1 => 0,
                2 => 0,
                3 => 0,
                4 => 0,
                5 => 0
            ],
            'alpha' => [
                0 => 0,
                1 => 0,
                2 => 0,
                3 => 0,
                4 => 0,
                5 => 0
            ]
        ];

        return $histArray;
    }

    /**
     * @param Picture $picture
     *
     * @return resource
     */
    private function getImage(Picture $picture)
    {
        $ext = pathinfo($picture->getPath(), PATHINFO_EXTENSION);

        switch($ext) {
            case 'jpg':
            case 'jpeg':
                $im = imagecreatefromjpeg($picture->getPath());
                break;
            case 'png':
                $im = imagecreatefrompng($picture->getPath());
                break;
            case 'gif':
                $im = imagecreatefromgif($picture->getPath());
                break;
            default:
                throw new \Exception('Unrecognized image type : ' . $picture->getPath());
        }
        return $im;
    }
}
