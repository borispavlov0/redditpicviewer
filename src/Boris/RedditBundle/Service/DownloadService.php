<?php
/**
 * @package RedditPicViewer
 */

namespace Boris\RedditBundle\Service;

use Boris\RedditBundle\Entity\Picture;
use Doctrine\ORM\EntityManager;
use Gregwar\ImageBundle\Services\ImageHandling;

class DownloadService
{
    /**
     * @var EntityManager
     */
    private $em;
    /**
     * @var string
     */
    private $downloadDir;
    /**
     * @var ImageHandling
     */
    private $imageHandling;

    /**
     * @param EntityManager $em
     * @param ImageHandling                  $imageHandling
     * @param                                $downloadDir
     */
    public function __construct(EntityManager $em, ImageHandling $imageHandling, $downloadDir)
    {
        $this->em = $em;
        $this->downloadDir = $downloadDir;
        $this->imageHandling = $imageHandling;
    }

    public function downloadImage($url)
    {
        $picture = $this->em->getRepository('BorisRedditBundle:Picture')->findOneBy(['source' => $url]);

        if (!$picture) {
            $picture = new Picture();
            $picture->setSource($url);
            $filename = $this->getFilename() . '.' . $this->getExtension($url);

            $this->imageHandling->open($url)->save($filename);

            $picture->setPath($filename);

            $this->em->persist($picture);
            $this->em->flush();
        }

        return $picture;
    }

    private function getExtension($url)
    {
        $parts = explode(".", $url);

        return end($parts);
    }

    private function getFilename()
    {
        return rtrim($this->downloadDir, '/') . '/' . uniqid();
    }
}
