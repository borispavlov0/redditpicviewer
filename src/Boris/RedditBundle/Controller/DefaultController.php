<?php

namespace Boris\RedditBundle\Controller;

use Boris\RedditBundle\Entity\Picture;
use Boris\RedditBundle\Service\CompareService;
use Boris\RedditBundle\Service\HistogramService;
use Boris\RedditBundle\Service\RedditService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction()
    {
        /** @var RedditService $redditService */
        $redditService = $this->container->get('boris.reddit');

        return $this->render('BorisRedditBundle:Default:index.html.twig', [
            'posts' => $redditService->getHotPosts('aww', 10)
        ]);
    }

    public function createHistogramAction($pictureId)
    {
        ini_set('max_execution_time', 300);
        /** @var HistogramService $histService */
        $histService = $this->container->get('histogram_service');
        /** @var Picture $picture */
        $picture = $this->getDoctrine()->getManager()->find('BorisRedditBundle:Picture', $pictureId);

        if ($picture) {
            $picture = $histService->addHistogram($picture);
            $this->getDoctrine()->getManager()->persist($picture);
            $this->getDoctrine()->getManager()->flush();
        }

        return $this->render('BorisRedditBundle:Default:hist.html.twig', [
            'picture' => $picture
        ]);
    }

    public function findSimilarAction(Request $request, $pictureId)
    {
        $offset = $request->query->get('offset') / 100;

        /** @var Picture $picture */
        $picture = $this->getDoctrine()->getManager()->find('BorisRedditBundle:Picture', $pictureId);
        /** @var CompareService $compareService */
        $compareService = $this->container->get('compare_service');

        return $this->render('BorisRedditBundle:Default:similar.html.twig', [
            'originalPicture' => $picture,
            'pictures'        => $compareService->findSimilar($picture, $offset)
        ]);
    }
}
