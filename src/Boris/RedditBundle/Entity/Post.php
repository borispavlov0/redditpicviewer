<?php

namespace Boris\RedditBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Post
 */
class Post
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $redditId;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set redditId
     *
     * @param string $redditId
     * @return Post
     */
    public function setRedditId($redditId)
    {
        $this->redditId = $redditId;

        return $this;
    }

    /**
     * Get redditId
     *
     * @return string 
     */
    public function getRedditId()
    {
        return $this->redditId;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $pictures;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->pictures = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add pictures
     *
     * @param \Boris\RedditBundle\Entity\Picture $pictures
     * @return Post
     */
    public function addPicture(\Boris\RedditBundle\Entity\Picture $pictures)
    {
        $this->pictures[] = $pictures;

        return $this;
    }

    /**
     * Remove pictures
     *
     * @param \Boris\RedditBundle\Entity\Picture $pictures
     */
    public function removePicture(\Boris\RedditBundle\Entity\Picture $pictures)
    {
        $this->pictures->removeElement($pictures);
    }

    /**
     * Get pictures
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPictures()
    {
        return $this->pictures;
    }
    /**
     * @var string
     */
    private $url;


    /**
     * Set url
     *
     * @param string $url
     * @return Post
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }
}
