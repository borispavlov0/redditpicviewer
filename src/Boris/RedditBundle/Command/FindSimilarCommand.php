<?php
/**
 * @package RedditPicViewer
 */

namespace Boris\RedditBundle\Command;

use Boris\RedditBundle\Entity\Picture;
use Boris\RedditBundle\Service\CompareService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

class FindSimilarCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('boris:reddit:similar')
            ->setDescription('Find similar pictures')
            ->addArgument(
                'percentage',
                InputArgument::OPTIONAL,
                'The percentage offset of difference between each RGB histogram',
                10
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var EntityManager $em */
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        /** @var QueryBuilder $qb */
        $qb = $em->getRepository('BorisRedditBundle:Histogram')->createQueryBuilder('h');

        $qb->join('h.picture', 'p')->select('p.id, p.source')->distinct();

        $pictures = $qb->getQuery()->getArrayResult();

        foreach ($pictures as $p) {
            $output->writeln(
                '    id ' . $p['id'] . ' : <comment>' . $p['source'] . '</comment>'
            );
        }

        $offset = $input->getArgument('percentage') / 100;
        /** @var QuestionHelper $helper */
        $helper = $this->getHelper('question');
        $question = new Question('Which picture do you want to compare?');

        $picId = $helper->ask($input, $output, $question);
        /** @var Picture $picture */
        $picture = $em->find('BorisRedditBundle:Picture', $picId);
        /** @var CompareService $compareService */
        $compareService = $this->getContainer()->get('compare_service');

        $similars = $compareService->findSimilar($picture, $offset);

        foreach ($similars as $p) {
            /** @var $p Picture */
            $output->writeln($p->getId());
        }
    }
}
