<?php
/**
 * @package RedditPicViewer
 */

namespace Boris\RedditBundle\Service;

use Boris\ImgScrape\Scraper;
use Boris\RedditBundle\Entity\Picture as PictureObject;

class PictureService
{
    /**
     * @var DownloadService
     */
    private $downloadService;
    /**
     * @var Scraper
     */
    private $scraper;
    /**
     * @var HistogramService
     */
    private $histService;

    /**
     * @param DownloadService  $downloadService
     * @param Scraper   $scraper
     * @param HistogramService $histService
     */
    public function __construct(DownloadService $downloadService, Scraper $scraper, HistogramService $histService)
    {

        $this->downloadService = $downloadService;
        $this->scraper = $scraper;
        $this->histService = $histService;
    }

    /**
     * @param string $url
     *
     * @return PictureObject|null
     */
    public function getPicture($url)
    {
        if ($imageUrl = $this->scraper->getLargestImageUrl($url)) {
            $picture = $this->downloadService->downloadImage($imageUrl);

            return $picture;
        }

        return null;
    }
}