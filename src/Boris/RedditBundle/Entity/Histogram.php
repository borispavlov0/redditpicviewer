<?php

namespace Boris\RedditBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Histogram
 */
class Histogram
{
    /**
     * @var integer
     */
    private $id;
    /**
     * @var string
     */
    private $color;
    /**
     * @var float
     */
    private $barOne = 0;
    /**
     * @var float
     */
    private $barTwo = 0;
    /**
     * @var float
     */
    private $barThree = 0;
    /**
     * @var float
     */
    private $barFour = 0;
    /**
     * @var float
     */
    private $barFive = 0;
    /**
     * @var Picture
     */
    private $picture;
    /**
     * @var float
     */
    private $barSix = 0;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set color
     *
     * @param string $color
     *
     * @return Histogram
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set barOne
     *
     * @param string $barOne
     *
     * @return Histogram
     */
    public function setBarOne($barOne)
    {
        $this->barOne = $barOne;

        return $this;
    }

    /**
     * Get barOne
     *
     * @return float
     */
    public function getBarOne()
    {
        return $this->barOne;
    }

    /**
     * Set barTwo
     *
     * @param string $barTwo
     *
     * @return Histogram
     */
    public function setBarTwo($barTwo)
    {
        $this->barTwo = $barTwo;

        return $this;
    }

    /**
     * Get barTwo
     *
     * @return float
     */
    public function getBarTwo()
    {
        return $this->barTwo;
    }

    /**
     * Set barThree
     *
     * @param string $barThree
     *
     * @return Histogram
     */
    public function setBarThree($barThree)
    {
        $this->barThree = $barThree;

        return $this;
    }

    /**
     * Get barThree
     *
     * @return float
     */
    public function getBarThree()
    {
        return $this->barThree;
    }

    /**
     * Set barFour
     *
     * @param string $barFour
     *
     * @return Histogram
     */
    public function setBarFour($barFour)
    {
        $this->barFour = $barFour;

        return $this;
    }

    /**
     * Get barFour
     *
     * @return float
     */
    public function getBarFour()
    {
        return $this->barFour;
    }

    /**
     * Set barFive
     *
     * @param string $barFive
     *
     * @return Histogram
     */
    public function setBarFive($barFive)
    {
        $this->barFive = $barFive;

        return $this;
    }

    /**
     * Get barFive
     *
     * @return float
     */
    public function getBarFive()
    {
        return $this->barFive;
    }

    /**
     * Set picture
     *
     * @param Picture $picture
     *
     * @return Histogram
     */
    public function setPicture(Picture $picture = null)
    {
        $this->picture = $picture;

        return $this;
    }

    /**
     * Get picture
     *
     * @return Picture
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * Set barSix
     *
     * @param string $barSix
     *
     * @return Histogram
     */
    public function setBarSix($barSix)
    {
        $this->barSix = $barSix;

        return $this;
    }

    /**
     * Get barSix
     *
     * @return float
     */
    public function getBarSix()
    {
        return $this->barSix;
    }

    /**
     * @param array $values
     */
    public function exchangeArray(array $values)
    {
        $this->setBarOne($values[0]);
        $this->setBarTwo($values[1]);
        $this->setBarThree($values[2]);
        $this->setBarFour($values[3]);
        $this->setBarFive($values[4]);
        $this->setBarSix($values[5]);
    }

    /**
     * @return array
     */
    public function getValueArray()
    {
        return [
            $this->getBarOne(),
            $this->getBarTwo(),
            $this->getBarThree(),
            $this->getBarFour(),
            $this->getBarFive(),
            $this->getBarSix()
        ];
    }
}
