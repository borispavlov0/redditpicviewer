$(document).ready(function () {

    $(".displayPic").on('click', previewDisplay);

    $(".hist-button").on('click', toggleHist);

    $('.hist-create-button').on('click', function () {
        var element = this;
        $(this).off('click');

        $(element).html('Loading ...');
        $(element).removeClass('hist-create-button');
        $.ajax({
            url: $(this).data('target')
        }).done(function (data) {
            $(element).parent().children('.histogram').html(data);
            $(element).html('Histogram');
            $(element).addClass('hist-button');
            $(element).on('click', toggleHist);

        }).fail(function (data) {
            $(element).html('Failed!');
        });
    });

    $(".similar-button").on('click', function () {
        var element = this;
        $.ajax({
            url: $(this).data('url')
        }).done(function (data) {
            $('#thumbs').html(data);
        });
    });

    $("#carousel").owlCarousel({
        'items': 5
    });

    function previewDisplay() {
        $("#display").html('<img src="' + $(this).data('source') + '">');
    }

    function toggleHist() {
        console.log('test');
        var pic = $(this).parent().children('.displayPic');
        var hist = $(this).parent().children('.histogram');

        if ($(pic).css('display') == 'none') {
            $(hist).fadeOut('slow', function () {
                $(pic).fadeIn();
            });
        } else {
            $(pic).fadeOut('slow', function () {
                $(hist).fadeIn();
            });
        }

    }
});
