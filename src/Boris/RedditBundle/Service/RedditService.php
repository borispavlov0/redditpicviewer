<?php

namespace Boris\RedditBundle\Service;

use Boris\RedditBundle\Entity\Post;
use Doctrine\ORM\EntityManager;
use GuzzleHttp\Client;

class RedditService
{
    /**
     * @var Client
     */
    private $client;
    /**
     * @var array
     */
    private $params;
    /**
     * @var EntityManager
     */
    private $em;
    /**
     * @var PictureService
     */
    private $picService;

    /**
     * @param Client          $client
     * @param EntityManager   $em
     * @param PictureService $picService
     * @param array           $params
     */
    public function __construct(Client $client, EntityManager $em, PictureService $picService, array $params)
    {
        $this->client = $client;
        $this->params = $params;
        $this->em = $em;
        $this->picService = $picService;
    }

    public function getHotPosts($subreddit, $limit = 5)
    {
        $response = $this->client->get('/api/me.json')->json();

        if (empty($response)) {
            $response = $this->login();
        }
        $hotPosts = $this->client->get("/r/{$subreddit}/hot.json", $this->getRequest($response, $limit))->json();
        $result = $this->getPostObjects($hotPosts);

        return $result;
    }

    /**
     * @return mixed
     */
    private function login()
    {
        $response = $this->client->post('/api/login',
            [
                'body' => [
                    'api_type' => 'json',
                    'user'     => $this->params['reddit_username'],
                    'passwd'   => $this->params['reddit_password'],
                    'rem'      => true
                ]
            ]
        )->json();

        return $response;
    }

    /**
     * @param $response
     *
     * @param $limit
     *
     * @return array
     */
    private function getRequest($response, $limit)
    {
        return [
            'headers' => [
                'X-Modhash' => $response['json']['data']['modhash']
            ],
            'query'   => [
                'limit' => $limit
            ]
        ];
    }

    /**
     * @param $hotPosts
     *
     * @return array
     */
    private function getPostObjects($hotPosts)
    {
        $result = [];

        foreach ($hotPosts['data']['children'] as $key => $post) {
            if ($this->isValid($post)) {
                $postObject = $this->getPostObject($post);
                $result[] = $postObject;
            }
        }
        return $result;
    }

    /**
     * @param $post
     *
     * @return bool
     */
    private function isValid($post)
    {
        return in_array($post['data']['domain'], ['imgur.com', 'i.imgur.com']) && strpos($post['data']['url'],
            '/a/') === false;
    }

    /**
     * @param $post
     *
     * @return Post|null|object
     */
    private function getPostObject($post)
    {
        $postObject = $this
            ->em
            ->getRepository("BorisRedditBundle:Post")
            ->findOneBy(['url' => $post['data']['url']]);

        if (!$postObject) {
            $postObject = new Post();
            $postObject->setRedditId($post['data']['id']);
            $postObject->setUrl($post['data']['url']);

            $picture = $this->picService->getPicture($postObject->getUrl());

            if($picture) {
                $postObject->addPicture($picture);
                $this->em->persist($picture);
            }

            $this->em->persist($postObject);
            $this->em->flush();
            return $postObject;
        }
        return $postObject;
    }
}
