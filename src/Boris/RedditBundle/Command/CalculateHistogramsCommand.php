<?php
/**
 * @package RedditPicViewer
 */

namespace Boris\RedditBundle\Command;

use Boris\RedditBundle\Entity\Picture;
use Boris\RedditBundle\Service\HistogramService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CalculateHistogramsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('boris:calculate:histograms')
            ->setDescription('Calculate color histograms of pictures that are missing this detail')
            ->addOption('all', null, InputOption::VALUE_NONE, 'Calculate histograms of all pictures.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $all = $input->getOption('all')
            ? true
            : false;

        $pictures = $this->getPictures();

        if (sizeof($pictures) == 0) {
            $output->writeln('No pictures missing a histogram in the db. Execute fetch or reload the page.');
            exit();
        }

        foreach ($pictures as $p) {
            $output->writeln(
                '    id ' . $p['id'] . ' : <comment>' . $p['source'] . '</comment>'
            );
        }

        if (!$all) {
            $helper = $this->getHelper('question');

            $question = new Question('Which picture should be calculated (enter id)?');

            $picId = $helper->ask($input, $output, $question);

            $picArray[] = $picId;
        } else {
            $picArray = array_map(function ($value) {
                return $value['id'];
            }, $pictures);
        }
        $this->computeHistogram($picArray);
    }

    /**
     *
     * @return array
     */
    protected function getPictures()
    {
        /** @var EntityManager $em */
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        /** @var QueryBuilder $qb */
        $qb = $em->getRepository('BorisRedditBundle:Picture')->createQueryBuilder('p');

        $qb->select('p.id, p.source')->leftJoin('p.histograms', 'h')->where('h.id IS NULL');

        $pictures = $qb->getQuery()->getArrayResult();

        return $pictures;
    }

    /**
     * @param $picArray
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    protected function computeHistogram($picArray)
    {
        /** @var EntityManager $em */
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        foreach ($picArray as $p) {
            /** @var Picture $picture */
            $picture = $em->find('BorisRedditBundle:Picture', $p);
            /** @var HistogramService $histService */
            $histService = $this->getContainer()->get('histogram_service');

            $histService->addHistogram($picture);

            $em->persist($picture);
            $em->flush();
        }
    }
}
