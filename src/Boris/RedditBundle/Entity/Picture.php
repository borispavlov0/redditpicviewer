<?php

namespace Boris\RedditBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Picture
 */
class Picture
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $path;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return Picture
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }
    /**
     * @var string
     */
    private $source;

    /**
     * Set source
     *
     * @param string $source
     * @return Picture
     */
    public function setSource($source)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get source
     *
     * @return string 
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $histograms;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->histograms = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add histograms
     *
     * @param \Boris\RedditBundle\Entity\Histogram $histograms
     * @return Picture
     */
    public function addHistogram(\Boris\RedditBundle\Entity\Histogram $histograms)
    {
        $this->histograms[] = $histograms;

        return $this;
    }

    /**
     * Remove histograms
     *
     * @param \Boris\RedditBundle\Entity\Histogram $histograms
     */
    public function removeHistogram(\Boris\RedditBundle\Entity\Histogram $histograms)
    {
        $this->histograms->removeElement($histograms);
    }

    /**
     * Get histograms
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHistograms()
    {
        return $this->histograms;
    }
}
