<?php
/**
 * @package RedditPicViewer
 */

namespace Boris\RedditBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FetchCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('boris:reddit:fetch')
            ->setDescription('Fetch the hottest pictures from a subreddit')
            ->addArgument(
                'subreddit',
                InputArgument::REQUIRED,
                'The name of the subreddit to scan?'
            )
            ->addArgument(
                'limit',
                InputArgument::OPTIONAL,
                'Default limit is 5 posts.'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $imageLinksOnly = $this->getContainer()->getParameter('boris.scraper')['imageLinksOnly']
            ? 'true'
            : 'false';

        $output->writeln(
            'using image links only: ' . $imageLinksOnly
        );

        $subreddit = $input->getArgument('subreddit');
        $limit = $input->getArgument('limit');

        $posts = $this->getContainer()->get('boris.reddit')->getHotPosts($subreddit, $limit);

        $output->writeln('returning ' . sizeof($posts) . ' posts');
    }
}
