<?php
/**
 * @package RedditPicViewer
 */

namespace Boris\RedditBundle\Service;

use Boris\RedditBundle\Entity\Histogram;
use Boris\RedditBundle\Entity\Picture;
use Doctrine\ORM\EntityManager;

class CompareService
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function findSimilar(Picture $picture, $offset = 0.1)
    {
        if (sizeof($picture->getHistograms()) == 0) {
            return [];
        }

        $params = $this->getQueryParameters($picture, $offset);

        $picIds = array_intersect(
            $this->getResults($params['red']),
            $this->getResults($params['blue']),
            $this->getResults($params['green'])
        );

        if (sizeof($picIds) == 0) {
            return [];
        }

        $pictures = $this->getPictureObjects($picIds);

        return $pictures;
    }

    /**
     * @param Picture $picture
     * @param         $color
     *
     * @return Histogram
     */
    private function getColorHistogram(Picture $picture, $color)
    {
        return $picture->getHistograms()->filter(function ($h) use ($color) {
            /** @var $h Histogram */
            return $h->getColor() == $color;
        })->first();
    }

    /**
     * @param float $offset
     * @param float $value
     *
     * @return float
     */
    private function getBottomBorder($offset, $value)
    {
        return ($value - $offset) >= 0
            ? ($value - $offset)
            : 0;
    }

    /**
     * @param float $offset
     * @param float $value
     *
     * @return float
     */
    private function getTopBorder($offset, $value)
    {
        return ($value + $offset);
    }

    /**
     * @param Histogram $hist
     * @param           $offset
     *
     * @return array
     */
    private function createParams(Histogram $hist, $offset)
    {
        return [
            'id'           => $hist->getPicture()->getId(),
            'color'        => $hist->getColor(),
            'one_bottom'   => $this->getBottomBorder($offset, $hist->getBarOne()),
            'one_top'      => $this->getTopBorder($offset, $hist->getBarOne()),
            'two_bottom'   => $this->getBottomBorder($offset, $hist->getBarTwo()),
            'two_top'      => $this->getTopBorder($offset, $hist->getBarTwo()),
            'three_bottom' => $this->getBottomBorder($offset, $hist->getBarThree()),
            'three_top'    => $this->getTopBorder($offset, $hist->getBarThree()),
            'four_bottom'  => $this->getBottomBorder($offset, $hist->getBarFour()),
            'four_top'     => $this->getTopBorder($offset, $hist->getBarFour()),
            'five_bottom'  => $this->getBottomBorder($offset, $hist->getBarFive()),
            'five_top'     => $this->getTopBorder($offset, $hist->getBarFive()),
            'six_bottom'   => $this->getBottomBorder($offset, $hist->getBarSix()),
            'six_top'      => $this->getTopBorder($offset, $hist->getBarSix()),
        ];
    }

    /**
     * @param Picture $picture
     * @param float   $offset
     *
     * @return array
     */
    private function getQueryParameters(Picture $picture, $offset)
    {
        /** @var Histogram $red */
        $red = $this->getColorHistogram($picture, 'red');
        /** @var Histogram $blue */
        $blue = $this->getColorHistogram($picture, 'blue');
        /** @var Histogram $green */
        $green = $this->getColorHistogram($picture, 'green');

        $params = [
            'red'   => $this->createParams($red, $offset),
            'blue'  => $this->createParams($blue, $offset),
            'green' => $this->createParams($green, $offset),
        ];

        return $params;
    }

    /**
     * @param array $params
     *
     * @return array
     */
    private function getResults($params)
    {
        $qb = $this->em->createQueryBuilder();
        $results = $qb->select('p.id')
            ->from('BorisRedditBundle:Histogram', 'h')
            ->join('h.picture', 'p')
            ->where('h.barOne BETWEEN :one_bottom AND :one_top')
            ->andWhere('h.barTwo BETWEEN :two_bottom AND :two_top')
            ->andWhere('h.barThree BETWEEN :three_bottom AND :three_top')
            ->andWhere('h.barFour BETWEEN :four_bottom AND :four_top')
            ->andWhere('h.barFive BETWEEN :five_bottom AND :five_top')
            ->andWhere('h.barSix BETWEEN :six_bottom AND :six_top')
            ->andWhere('p.id != :id')
            ->andWhere('h.color = :color')
            ->setParameters($params)
            ->getQuery()
            ->getArrayResult();

        return array_column($results, 'id');
    }

    /**
     * @param $picIds
     *
     * @return array
     */
    private function getPictureObjects($picIds)
    {
        $pictures = $this->em->getRepository('BorisRedditBundle:Picture')
            ->createQueryBuilder('p')
            ->where('p.id IN (:pic_ids)')
            ->setParameter('pic_ids', $picIds)
            ->getQuery()
            ->getResult();
        return $pictures;
    }
}